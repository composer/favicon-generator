<?php
namespace Hypermetrica\Favicon\Exception;

use \RuntimeException;

/**
 * Class GeneratorException
 * @package Hypermetrica\Favicon\Exception
 */
class GeneratorException extends RuntimeException
{
}
