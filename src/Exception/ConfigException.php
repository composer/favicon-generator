<?php
namespace Hypermetrica\Favicon\Exception;

use \Exception;

/**
 * Class ConfigException
 * @package Hypermetrica\Favicon\Exception
 */
class ConfigException extends Exception
{
}
